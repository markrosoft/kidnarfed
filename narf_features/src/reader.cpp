#include <iostream>
#include "ros/ros.h"
#include <ros/time.h>

#include "sensor_msgs/PointCloud2.h"
#include <pcl/features/narf_descriptor.h>
#include <pcl/features/narf.h>

#include <pcl/features/narf.h>
#include <std_msgs/Float32.h>

#include <vector>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_statistics.h>

#include <visualization_msgs/MarkerArray.h>
#include <std_msgs/Float32MultiArray.h>

using namespace std;
vector<vector<float> > Array(1000, vector<float>(36));

bool sampled = false;
int ii = 0;

double 	threshold 	= 0.99;		// High number as to not muddy the water
int 	repetition 	= 50;		// At least how many matches should I see
int 	BOW 		= 250;		// How many sets?

class NarfLexicon {
        protected:

                ros::Subscriber sub;
                ros::NodeHandle n;
                ros::Publisher pub;
        public:
 
                NarfLexicon(ros::NodeHandle* _n)
                {
						this->n = *_n;
						this->sub = this->n.subscribe("camera/depth/features", 3, &NarfLexicon::trainingCallBack, this);
						ROS_INFO("Sampling Data");
				}

				void trainingCallBack(const sensor_msgs::PointCloud2::ConstPtr& narf_msg)
				{
					pcl::PointCloud<pcl::Narf36> cloud;
					pcl::fromROSMsg (*narf_msg, cloud);

					
					//cout << *test << " : " << 
					for(int j = 0; j < cloud.size();j++)
					{	
						if(ii>=1000)
						{
							this->sub = this->n.subscribe("camera/depth/features", 1, &NarfLexicon::KmeansCallback, this);
							ROS_INFO("Subscriber changed, Processing Data");
							break;
						}
						else
						{
							for(int i = 0; i < 36; i++)
							{	
								Array[ii][i] = cloud[j].descriptor[i];
							}
						}
							ii++;
						  	cout << ii << " | 1000" << endl;
							cout << "\033[F";
						}                                                                                                       
		 
                	}

		void KmeansCallback(const sensor_msgs::PointCloud2::ConstPtr& narf_msg)
		{

			pcl::PointCloud<pcl::Narf36> cloud;
			pcl::fromROSMsg (*narf_msg, cloud);

			int arbitaryCount = 0;
			if(sampled)
			{
				ROS_INFO("Maintaining List and visualising");
				this->sub = this->n.subscribe("camera/depth/features", 1, &NarfLexicon::MaintainingList, this);
			}
			else
			{

				ROS_INFO("performing BIG gsl (Pearson) correlation");
				int tempAr[1000];
				for(int i = 0; i < 1000; i++){tempAr[i]=0;}
	  			size_t n = 36;

	  			int slowloop  = 1;
	  			int quickloop = 0;
	  			double gsl_comparason;

	  			while(slowloop < 1000)
	  			{
	  				vector<double> x;

	  				for(int i = 0; i<n;i++)
		  			{
		  				x.push_back(static_cast<double>(Array[slowloop][i]));
		  			}

		  			quickloop = 1;
	  				while(quickloop < 999)
	  				{
	  					vector<double> y;
			  			for(int i = 0; i<36;i++)
			  			{
			  				y.push_back(static_cast<double>(Array[quickloop][i]));
			  			}

			  			// show vector match
						//for(size_t i=0; i<n; ++i)
			      		//	printf ("x[%ld]=%.5f y[%ld]=%.5f\n", i, x[i], i, y[i]);
						//printf("\n");
						  			
			  			gsl_vector_const_view gsl_x = gsl_vector_const_view_array(&x[0], x.size() );
			  			gsl_vector_const_view gsl_y = gsl_vector_const_view_array(&y[0], y.size() );

			  			gsl_comparason = gsl_stats_correlation( (double*) gsl_x.vector.data, 1,
			                                    (double*) gsl_y.vector.data, 1, n );
			  						 //printf ("Pearson correlation = %f\n", gsl_comparason);
			  			quickloop++;
						if(gsl_comparason > threshold)
			  			{
			  				tempAr[quickloop]++;
			  				tempAr[slowloop]++;
			  			}
			  		}
					arbitaryCount = gsl_comparason > threshold ? arbitaryCount++ : arbitaryCount;
			  		slowloop++;
		  		}
				ROS_INFO("Trimming Noise");
		  	// Greater than Method
		  	//	for(int i = Array.size(); i > 0; i--){
		  	//		if(tempAr[i] < repetition)
		  	//		{
		  	//			Array.erase (Array.begin()+i);
		  	//		}
		  	//	}

			// Highest output Clusters.
				int max;
				for(int i = 0; i < BOW; i++)
				{
					max = 0;
					for(int j = 0; j < Array.size(); j++)
					{
						
						max = (tempAr[max] < tempAr[j]) ? j : max;

					}
					//printf("%d\n", max);
					tempAr[max] = -1;
				}
		  		for(int i = Array.size(); i > 0; i--){
		  			if(tempAr[i] != -1)
		  			{
		  				Array.erase (Array.begin()+i);
		  			}
		  		}


		  		ROS_INFO("Noise Trimmed, Created %d Lexicon BOW Dictionary",(int)Array.size());
		  		sampled = true;	 
		}
	  		
			

			/*labels.set(CV_CAP_PROP_FRAME_WIDTH, 640);
			labels.set(CV_CAP_PROP_FRAME_HEIGHT, 480); 

			centers.set(CV_CAP_PROP_FRAME_WIDTH, 640);
			centers.set(CV_CAP_PROP_FRAME_HEIGHT, 480); 
*/
			//kmeans(Array, 20, labels, cv::TermCriteria(CV_TERMCRIT_ITER|CV_TERMCRIT_EPS, 10000, 0.0001), attempts, cv::KMEANS_PP_CENTERS,centers);
 		}
		void MaintainingList(const sensor_msgs::PointCloud2::ConstPtr& narf_msg)
        {
	    	pcl::PointCloud<pcl::Narf36> cloud;
			pcl::fromROSMsg (*narf_msg, cloud);


			size_t n = 36;
	       	//printf("%d\n", (int)cloud.size());
	       	int matches = 0;
	       	for(int j = 0; j < (int)cloud.size();j++)
	       	{
				vector<double> x;

				// Assign incoming narf feature
				for(int i = 0; i<n;i++)
				{        
					x.push_back(static_cast<double>(cloud[j].descriptor[i]));
				}

				// Compair against those in Array
				for(int i = Array.size(); i > 0; i--)
				{	
					vector<double> y;
					for(int ii = 0; ii<36;ii++)
					{
						y.push_back(static_cast<double>(Array[i][ii]));
					}

					gsl_vector_const_view gsl_x = gsl_vector_const_view_array(&x[0], x.size() );
					gsl_vector_const_view gsl_y = gsl_vector_const_view_array(&y[0], y.size() );

					double gsl_comparason = gsl_stats_correlation((double*) gsl_x.vector.data, 1,
								(double*) gsl_y.vector.data, 1, n );


			      	if(gsl_comparason > threshold)
			        {
			        	matches++;
					//	printf("%d looks like %d\n", j, i);
					/*	printf("%2f %2f %2f %2f %2f %2f %2f %2f %2f %2f        %2f %2f %2f %2f %2f %2f %2f %2f %2f %2f        %2f %2f %2f %2f %2f %2f %2f %2f %2f %2f        %2f %2f %2f %2f %2f %2f\n", 
				             (float)x[0],(float)x[1], (float)x[2], (float)x[3], (float)x[4], (float)x[5], (float)x[6], (float)x[7], (float)x[8], (float)x[9],
				             (float)x[10],(float)x[11], (float)x[12], (float)x[13], (float)x[14], (float)x[15], (float)x[16], (float)x[17], (float)x[18], (float)x[19],
				             (float)x[20],(float)x[21], (float)x[22], (float)x[23], (float)x[24], (float)x[25], (float)x[26], (float)x[27], (float)x[28], (float)x[29],
				             (float)x[30],(float)x[31], (float)x[32], (float)x[33], (float)x[34], (float)x[35]);
	
						printf("%2f %2f %2f %2f %2f %2f %2f %2f %2f %2f        %2f %2f %2f %2f %2f %2f %2f %2f %2f %2f        %2f %2f %2f %2f %2f %2f %2f %2f %2f %2f        %2f %2f %2f %2f %2f %2f\n", 
							(float)y[0],(float)y[1], (float)y[2], (float)y[3], (float)y[4], (float)y[5], (float)y[6], (float)y[7], (float)y[8], (float)y[9],
							(float)y[10],(float)y[11], (float)y[12], (float)y[13], (float)y[14], (float)y[15], (float)y[16], (float)y[17], (float)y[18], (float)y[19],
							(float)y[20],(float)y[21], (float)y[22], (float)y[23], (float)y[24], (float)y[25], (float)y[26], (float)y[27], (float)y[28], (float)y[29],
							(float)y[30],(float)y[31], (float)y[32], (float)y[33], (float)y[34], (float)y[35]);
							*/
					}
				}

						  	cout << matches << " | Strong Matches, based upon " << (int)cloud.size() << " features" << endl;
						  	cout << "\033[F";

			}
			
			

		//Technique for finding XYZ
		//	cout << *reinterpret_cast<const float*>(&narf_msg->data[0]) << " | " << *reinterpret_cast<const float*>(&narf_msg->data[4]) << " | " << *reinterpret_cast<const float*>(&narf_msg->data[8]) << endl;
		//	cout << *reinterpret_cast<const float*>(&narf_msg->data[168]) << " | " << *reinterpret_cast<const float*>(&narf_msg->data[172]) << " | " << *reinterpret_cast<const float*>(&narf_msg->data[176]) << endl;


		}
};
 
int main(int argc, char **argv)
{
 
        ros::init(argc, argv, "NarfReader");
        ros::NodeHandle n;

        NarfLexicon e(&n);
        ros::spin();
 		std::cout << "\nend" << std::endl;
}
