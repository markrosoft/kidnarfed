#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "iostream"
#include <iostream>
#include "ros/ros.h"
#include <ros/time.h>

#include "sensor_msgs/PointCloud2.h"
#include <pcl/features/narf_descriptor.h>
#include <pcl/features/narf.h>

#include <pcl/features/narf.h>
#include <std_msgs/Float32.h>
#include <vector>

using namespace cv;

vector<vector<float> > Array(10000, vector<float>(36));
void show_result(const cv::Mat&, const cv::Mat&, int);
int KMeans();
cv::Mat *matrix = new cv::Mat(36, 10000, DataType<float>::type, cv::Scalar(0));

int ii;
int clusters = 10;
typedef Matx<float, 1, 2> Matx12f;

class NarfLexicon {
	protected:

		ros::Subscriber sub;
		ros::NodeHandle n;
		ros::Publisher pub;
	public:
 
	NarfLexicon(ros::NodeHandle* _n)
	{
		this->n = *_n;
		this->sub = this->n.subscribe("camera/depth/features", 3, &NarfLexicon::trainingCallBack, this);
		ROS_INFO("Sampling Data");
		}
	
        void trainingCallBack(const sensor_msgs::PointCloud2::ConstPtr& narf_msg)
	{
		pcl::PointCloud<pcl::Narf36> cloud;
		pcl::fromROSMsg (*narf_msg, cloud);
		
		//cout << *test << " : " << 
		for(int j = 0; j < cloud.size();j++)
		{       
			if(ii>=10000)
			{
				this->sub = this->n.subscribe("camera/depth/features", 1, &NarfLexicon::KmeansCallback, this);
				ROS_INFO("Recorded 1000, Performing K-Means");
				break;
			}
			else
			{
				for(int i = 0; i < 36; i++)
				{       
					Array[ii][i] = cloud[j].descriptor[i];
				}
		        }
			ii++;
			cout << ii << " | 1000" << endl;
			cout << "\033[F";
		}												       
		 
	}
	
        void KmeansCallback(const sensor_msgs::PointCloud2::ConstPtr& narf_msg)
	{
		cv::Mat image = Mat::zeros(Array.size(), Array[0].size(), cv::DataType<float>::type);
		for (int i=0; i<Array.size(); i++)
        	{
			for (int j=0; j<Array[i].size(); j++)
			{
			image.at<float>(i,j) = Array[i][j];
			//cout << vect[i][j] << " ";
			}	  
		}
				int cluster_number = clusters;
			//cv::Mat image = Mat(Array);
			std::cout << "image: " << image.rows << ", " << image.cols << std::endl;
		assert(image.type() == CV_32F);
		
	 
		cv::Mat reshaped_image = image;
		std::cout << "reshaped image: " << reshaped_image.rows << ", " << reshaped_image.cols << std::endl;
		assert(reshaped_image.type() == CV_32F);
		//check0(image, reshaped_image);
		 cv::imshow("image", reshaped_image);
		cv::Mat reshaped_image32f;
		reshaped_image.convertTo(reshaped_image32f, CV_32F, 1.0 / 255.0);
		std::cout << "reshaped image 32f: " << reshaped_image32f.rows << ", " << reshaped_image32f.cols << std::endl;
		assert(reshaped_image32f.type() == CV_32F);
		 
		cv::Mat labels;
		cv::TermCriteria criteria(cv::TermCriteria::COUNT, 100, 1);
		cv::Mat centers;
		cv::kmeans(reshaped_image32f, clusters, labels, criteria, 1, cv::KMEANS_RANDOM_CENTERS, centers);
	 
		
		for(int i = 0; i < clusters; i++)
		{
			for(int j = 0; j < 36; j++)
			{
				printf("%5f\t", centers.at<float>(i,j));
			}
			printf("\n");
		}
		//show_result(labels, centers, image.rows);
                cout << clusters;
                this->sub = this->n.subscribe("camera/depth/features", 1, &NarfLexicon::DummySub, this);
                ROS_INFO("KMeans Achieved");

	}
        void DummySub(const sensor_msgs::PointCloud2::ConstPtr& narf_msg)
        {
                printf(".");
        }
};

int main(int argc, char **argv)
{
	if (argc > 1)
	{
		std::istringstream in(argv[1]);
		clusters = 2;
		if(!(in >> clusters && in.eof()))
		{
			clusters = 10;
		}
	}
	ros::init(argc, argv, "NarfReader");
	ros::NodeHandle n;

	NarfLexicon e(&n);
	ros::spin();
	
	std::cout << "\nend" << std::endl;


     //  KMeans(25);
	
}