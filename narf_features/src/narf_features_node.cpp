#include <iostream>
#include "ros/ros.h"
#include <ros/time.h>
#include "sensor_msgs/PointCloud2.h"

#include "message_filters/subscriber.h"
#include <boost/thread/thread.hpp>

#include <pcl_ros/transforms.h>
#include <pcl/ros/conversions.h>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/range_image/range_image.h>
#include <pcl/range_image/range_image_planar.h>
#include <pcl/io/pcd_io.h>
#include <pcl/visualization/range_image_visualizer.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/features/range_image_border_extractor.h>
#include <pcl/keypoints/narf_keypoint.h>
#include <pcl/features/narf_descriptor.h>
#include <pcl/console/parse.h>
#include <pcl/ros/conversions.h> 
#include "std_msgs/UInt8MultiArray.h"


#include <pcl_ros/point_cloud.h>
#include <pcl/features/narf.h>
//

#include <pcl/features/range_image_border_extractor.h>


typedef pcl::PointXYZ PointType;
typedef pcl::PointCloud<pcl::PointCloud<int> > PointCloud;


ros::Publisher pub;

pcl::PointCloud<int> keypoint_indices;

pcl::PointCloud<pcl::PointXYZ>::Ptr keypoints_ptr (new pcl::PointCloud<pcl::PointXYZ>);
pcl::PointCloud<pcl::PointXYZ>& keypoints = *keypoints_ptr;


float angular_resolution_;
float support_size_ =0.2f;//-0.1f
// --------------------
// -----Parameters-----
// --------------------
float angular_resolution =pcl::deg2rad(0.5);
float support_size = 0.2f;
pcl::RangeImage::CoordinateFrame coordinate_frame = pcl::RangeImage::CAMERA_FRAME;
bool setUnseenToMaxRange = false;
bool rotation_invariant = true;
float noise_level = 0.0;
  float min_range = 0.0f;
  int border_size = 1;
Eigen::Affine3f scene_sensor_pose (Eigen::Affine3f::Identity ());


void imgcb(const sensor_msgs::PointCloud2::ConstPtr& depth_msg)
{
 const uint16_t* depthImage = reinterpret_cast<const uint16_t*>(&depth_msg->data[0]);
 //const uint16_t* depthImage = reinterpret_cast<const uint16_t*>(&depth_msg->data[0]);
	pcl::PointCloud<pcl::PointXYZ> point_cloud;
  	pcl::fromROSMsg (*depth_msg, point_cloud);
	pcl::RangeImage range_image;
	point_cloud.width = (int) point_cloud.points.size ();  point_cloud.height = 1;
	range_image.createFromPointCloud (point_cloud, angular_resolution, pcl::deg2rad (360.0f), pcl::deg2rad (180.0f),
                                   scene_sensor_pose, coordinate_frame, noise_level, min_range, border_size);

	pcl::RangeImageBorderExtractor range_image_border_extractor_;

	// convert the depth-image to a pcl::rangeImage
	//angular_resolution_ = (float)(-1);
	//range_image.setDepthImage(depthImage,depth_msg->width, depth_msg->height, (depth_msg->width)/2, (depth_msg->height)/2, 640.0, 480.0, angular_resolution_);

	//range_image.setUnseenToMaxRange();
	// --------------------------------
	// -----Extract NARF keypoints-----
	// --------------------------------
	pcl::RangeImageBorderExtractor range_image_border_extractor;
	pcl::NarfKeypoint narf_keypoint_detector;
	narf_keypoint_detector.setRangeImageBorderExtractor (&range_image_border_extractor);
	narf_keypoint_detector.setRangeImage (&range_image);
	narf_keypoint_detector.getParameters ().support_size = support_size;
	
	pcl::PointCloud<int> keypoint_indices;
	narf_keypoint_detector.compute (keypoint_indices);
	//std::cout << "Found "<<keypoint_indices.points.size ()<<" key points.\n";

	// ------------------------------------------------------
	// -----Extract NARF descriptors for interest points-----
	// ------------------------------------------------------
	std::vector<int> keypoint_indices2;
	keypoint_indices2.resize (keypoint_indices.points.size ());
	for (unsigned int i=0; i<keypoint_indices.size (); ++i) // This step is necessary to get the right vector type
		keypoint_indices2[i]=keypoint_indices.points[i];
	pcl::NarfDescriptor narf_descriptor (&range_image, &keypoint_indices2);
	narf_descriptor.getParameters ().support_size = support_size;
	narf_descriptor.getParameters ().rotation_invariant = rotation_invariant;
	pcl::PointCloud<pcl::Narf36> narf_descriptors;
	narf_descriptors.header=depth_msg->header;
	narf_descriptor.compute (narf_descriptors);
	std::cout << "Extracted "<<narf_descriptors.size ()<<" descriptors for " <<keypoint_indices.points.size ()<< " keypoints.\n";
	
	//cout << narf_descriptors[1].x <<  " | " << narf_descriptors[1].y <<  " | "<< narf_descriptors[1].z <<  endl;

	pub.publish (narf_descriptors);
	//sleep(5);
}

int main(int argc, char* argv[])
{
		ros::init(argc, argv, "narf");
		
		ros::NodeHandle nh;
		ros::NodeHandle n;
//		ros::Subscriber sub = nh.subscribe("head_xtion/depth/points", 1, imgcb);
		ros::Subscriber sub = nh.subscribe("camera/depth/points", 3, imgcb);
		//ros::Subscriber sub = nh.subscribe("head_xtion/depth/points", 1, imgcb);
		string pc_pub_topic;

		pub = nh.advertise<pcl::PointCloud<pcl::Narf36> > ("/camera/depth/features", 1);
		ros::spin();


		std::cout << "end" << std::endl;

		return 0;
}

