#include <iostream>
#include <dirent.h>
#include <ros/ros.h>
#include <rosbag/bag.h>
#include <rosbag/view.h>
#include <ros/time.h>
#include <ros/header.h>
#include <fstream>
#include <boost/foreach.hpp>
#include <pcl/keypoints/narf_keypoint.h>
#include <pcl/features/narf_descriptor.h>
#include <pcl/features/narf.h>
#include <pcl/range_image/range_image.h>
#include <pcl/range_image/range_image_planar.h>
#include <pcl/features/range_image_border_extractor.h>
#include <vector>
#include <algorithm>

// Bag Message Types //
#include <sensor_msgs/PointCloud2.h>
#include <std_msgs/Int16.h>

float angular_resolution_;
float support_size_ =0.2f; //-0.1f

// Narf Params
float angular_resolution = pcl::deg2rad(0.5);
float support_size = 0.2f;
pcl::RangeImage::CoordinateFrame coordinate_frame = pcl::RangeImage::CAMERA_FRAME;
bool setUnseenToMaxRange = false;
bool rotation_invariant = true;
float noise_level = 0.0;
float min_range = 0.0f;
int border_size = 1;
Eigen::Affine3f scene_sensor_pose (Eigen::Affine3f::Identity ());


int main(int argc, char* argv[])
{
	if (argc != 2)		// Query Terminal Input
   	{
     	std::cout << "Add Location of bags. (/home/strands/...bag)." << std::endl; 
     	return 0;
  	}
	
	ros::init(argc, argv, "bags");

/*	
	ifstream infile( "/home/strands/Desktop/octoberFail.txt" );
	vector<string> octoberBrokenLinks;
	while( infile.good() )
	{
		string substr;
		getline( infile, substr, ',' );
		octoberBrokenLinks.push_back( substr );
	}
*/
 	char filename[150];
    strcpy(filename, argv[1]);

	rosbag::Bag bag;

	DIR *dir;
	struct dirent *ent;
	char name[200];

	ros::Time::init();
	rosbag::Bag myBag;

	char bagName[150];
    sprintf(bagName,"/home/strands/NarfBag.bag");

	myBag.open(bagName, rosbag::bagmode::Write);
	int count = 0;
	int bagCount = 0;
	//int someCount=0;
	if ((dir = opendir (filename)) != NULL) {
		while ((ent = readdir (dir)) != NULL )
		{
			sprintf(name,"%s/%s", filename, ent->d_name);

  			std::string killNonFile = name;

		 	std::string temp = killNonFile.substr(killNonFile.find_last_of(".") + 1);
		 	int tempVal = 0;
			
			//for(int i = 0; i < octoberBrokenLinks.size();i++)
			//{
			//	if(octoberBrokenLinks[i] == name){tempVal=1; break;}
			//}

			// Parse only ".bag" files
			if(temp == "bag" && tempVal == 0)
			{
				// Inturpret which bag to use.
				int bagNum;
				istringstream (((std::string)name).substr((((std::string)name).find_last_of("_") + 1),1)) >> bagNum;
				
				std::cout << name << "\t";

				// Open Bags
				bag.open(name, rosbag::bagmode::Read);

				// define topics to sub too
				vector<std::string> topics;
				topics.push_back(std::string("/head_xtion/depth/points"));
			
				rosbag::View view(bag, rosbag::TopicQuery(topics));
				bagCount+=view.size();

				// Iterate through messages in bag
				BOOST_FOREACH(rosbag::MessageInstance const m, view)
				{
					// Check for topic (/head_xtion/depth/points)
					try
					{
						sensor_msgs::PointCloud2::Ptr s = m.instantiate<sensor_msgs::PointCloud2>();
						  
						pcl::PointCloud<pcl::PointXYZ> point_cloud;
				  		pcl::fromROSMsg (*s, point_cloud);

						pcl::RangeImage range_image;
						point_cloud.width = (int) point_cloud.points.size ();  point_cloud.height = 1;
						range_image.createFromPointCloud (point_cloud, angular_resolution, pcl::deg2rad (360.0f), pcl::deg2rad (180.0f),
									   scene_sensor_pose, coordinate_frame, noise_level, min_range, border_size);

						pcl::RangeImageBorderExtractor range_image_border_extractor_;

						// Extract NARF keypoints
						pcl::RangeImageBorderExtractor range_image_border_extractor;
						pcl::NarfKeypoint narf_keypoint_detector;
						narf_keypoint_detector.setRangeImageBorderExtractor (&range_image_border_extractor);
						narf_keypoint_detector.setRangeImage (&range_image);
						narf_keypoint_detector.getParameters ().support_size = support_size;
		
						pcl::PointCloud<int> keypoint_indices;
						narf_keypoint_detector.compute (keypoint_indices);

						// Extract NARF descriptors for interest points
						std::vector<int> keypoint_indices2;
						keypoint_indices2.resize (keypoint_indices.points.size ());
						for (unsigned int i=0; i<keypoint_indices.size (); ++i){ // This step is necessary to get the right vector type
							keypoint_indices2[i]=keypoint_indices.points[i];}

						pcl::NarfDescriptor narf_descriptor (&range_image, &keypoint_indices2);
						narf_descriptor.getParameters ().support_size = support_size;
						narf_descriptor.getParameters ().rotation_invariant = rotation_invariant;
						pcl::PointCloud<pcl::Narf36> narf_descriptors;
						
						narf_descriptor.compute (narf_descriptors);
						std::cout << "| Extracted "<<narf_descriptors.size ()<<" descriptors for " <<keypoint_indices.points.size ()<< " keypoints.";
						count+=narf_descriptors.size();

						// Populate Output Bags
						s->header.frame_id = "/camera_depth_frame";
						s->header.stamp = ros::Time();
						myBag.write("/cloud", ros::Time::now(), *s);

						sensor_msgs::PointCloud2 narf;
						pcl::toROSMsg(narf_descriptors,narf);
						
						std_msgs::Int16 BagPos;
						BagPos.data = bagNum;
						myBag.write("/bagNum", ros::Time::now(), BagPos);
						myBag.write("/camera/depth/features", ros::Time::now(), narf);					
					}
					catch (const rosbag::BagFormatException& e)
					{
						cout << "| " << e.what() << "!";
						continue;
					}
				} 
				std::cout << std::endl;  
			}
		    bag.close();			
		}
	}
	std::cout << " - Created output Bag. -\nFound "<< count << " unique Features. From " << bagCount << " Extractable bags." << std::endl;
	myBag.close();
}