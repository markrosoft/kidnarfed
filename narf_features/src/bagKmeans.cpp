#include <iostream>
#include <dirent.h>
#include <ros/ros.h>
#include <rosbag/bag.h>
#include <rosbag/view.h>
#include <ros/time.h>
#include <ros/header.h>
#include <fstream>
#include <boost/foreach.hpp>
#include <pcl/keypoints/narf_keypoint.h>
#include <pcl/features/narf_descriptor.h>
#include <pcl/features/narf.h>
#include <vector>
#include <algorithm>

// Bag Message Types //
#include <sensor_msgs/PointCloud2.h>
#include <std_msgs/Int16.h>

// Kmeans bag //
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"
std::vector<cv::Mat> kmeans();

int clusters = 50;		// Number of clusters //


size_t n = 36;
int bagNum = 0;

// Main Vector for Kmeans
typedef vector<vector<float> > narfFeature;
narfFeature vectStruct(0, vector<float>(n));
narfFeature vect;
std::vector<narfFeature> LocationArr;

int main(int argc, char* argv[])
{
	if (argc != 2)				// Has folder DIR been indicated? 				//
   	{
     	std::cout << "Add Location of bags. (/home/strands/...bag)." << std::endl; 
     	return 0;
  	}
	
	ros::init(argc, argv, "bagKmean");

 	char filename[150];	
    strcpy(filename, argv[1]);	// Pass Terminal Argument to string				//
	DIR *dir;					// New Directory								//
	struct dirent *ent;			// File in focus show cased by *ent				//
	char name[200];				// Handle File names up to 200 Char in Length	//
	rosbag::Bag bag;			// New ros bag object							//
	int bagLocation = 0;

	if ((dir = opendir (filename)) != NULL) {
		while ((ent = readdir (dir)) != NULL )
		{
			sprintf(name,"%s/%s", filename, ent->d_name);

		 	// Read only ".bag" files //
			if("bag" == string(name).substr(string(name).find_last_of(".") + 1))
			{
				std::cout << name << endl;	// Print 

				// Open Bags
				bag.open(name, rosbag::bagmode::Read);

				// define topics to sub too
				vector<std::string> topics;
				topics.push_back(std::string("/bagNum"));
				topics.push_back(std::string("/camera/depth/features"));
				rosbag::View view(bag, rosbag::TopicQuery(topics));

				// Iterate through messages in bag
				BOOST_FOREACH(rosbag::MessageInstance const m, view)
				{
					try // Handle Bag Header Read Exceptions 		//
					{
						// Extract Bag topics //
						std_msgs::Int16::Ptr c 	= m.instantiate<std_msgs::Int16>();
						sensor_msgs::PointCloud2::Ptr s = m.instantiate<sensor_msgs::PointCloud2>();

						// Run if std_msgs::Int16 message in scope //
						if(c != NULL)
						{
							bagLocation = c->data;
							while(LocationArr.size() < bagLocation +1)
							{
								LocationArr.push_back(vectStruct); // resize (handling more capture locations) //
							}	
						}
						
						// Run if sensor_msgs::PointCloud2 message in scope //
						if(s != NULL)
						{
							pcl::PointCloud<pcl::Narf36> narf;
							pcl::fromROSMsg (*s, narf);

							for(int j = 0; j < (int)narf.size();j++)
					       	{
								vector<float> x(n);
								// Assign incoming narf features	//
								for(int i = 0; i<n;i++)
								{        
									x[i] = narf[j].descriptor[i];
								}
								vect.push_back(x);
								LocationArr[bagLocation].push_back(x);
							}
						}
					}
					catch (const rosbag::BagFormatException& e)	// Catch all bag exceptions //
					{
						cout << "| " << e.what() << "!";
						continue;
					}
				} 
				std::vector<cv::Mat> j;
				kmeans();
			}
		    bag.close();			
		}
	}
	ROS_INFO("Bye");
}

std::vector<cv::Mat> kmeans()
{
	for(int i = 0; i < LocationArr.size(); i++)
	{
		cv::Mat image = cv::Mat::zeros(LocationArr[i].size(), LocationArr[i][0].size(), cv::DataType<float>::type);
			
		for (int j=0; j<LocationArr[i].size(); j++)
	    {
			for (int k=0; k<LocationArr[i][j].size(); k++)
			{
				image.at<float>(j,k) = LocationArr[i][j][k];
			}	  
		}
			//cv::Mat image = Mat(vect);
		//std::cout << "image: " << image.rows << ", " << image.cols << std::endl;
		assert(image.type() == CV_32F);
		
		cv::Mat labels;
		cv::TermCriteria criteria(cv::TermCriteria::COUNT, 100, 1);
		cv::Mat centers;
		double compactness = cv::kmeans(image, clusters, labels, criteria, 1, cv::KMEANS_RANDOM_CENTERS, centers);
		
		cout << LocationArr[i].size() << " Narf features clustered into " << clusters << " Clusters with a compactness of: " << compactness << endl;
	//	for(int i = 0; i < clusters; i++)
		{
	//		for(int j = 0; j < n; j++)
			{
				//printf("%5f\t", centers.at<float>(i,j));
			}
			//printf("\n");
		}
	}
	printf("%d", clusters);
	std::vector<cv::Mat> l;
	return l;
}