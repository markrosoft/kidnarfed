KidNarfed
=========

Analysing stable 3D features recorded by a robot, by clustering data recorded over the space of a month to attempt to find stable features, an initial step into the end goal of a 3D feature map to solve the Kidnapped robot problem using NARF 3D features. 
